/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.vladbutnaru.eventius.api;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.*;

import org.hibernate.Query;
import org.hibernate.Session;
import ro.vladbutnaru.eventius.persistence.HibernateUtil;

/**
 *
 * @author Vlad Butnaru
 */

  @Path("/cities")
  public class Cities {
    
  
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public City insertCity(City city) {
      
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        
        session.save(city);
        session.getTransaction().commit();
        session.close();
        return city;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<City> getCities(){
    
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from City"); 
        List<City> cities = query.list();
        return cities;
    
    
    }
    
    
    
}
